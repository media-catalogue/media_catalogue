pub mod domain;
pub mod repository;
use std::fmt::Debug;

#[derive(Debug)]
pub enum Error {
    Repository(RepositoryError),
}

#[derive(Debug)]
pub enum RepositoryError {
    DocumentAlreadyExist,
    DocumentDontExist
}

impl Error {
    pub fn new_repository_error(error: RepositoryError) -> Self {
        Self::Repository(error)
    }
}
