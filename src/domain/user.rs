use derive_getters::Getters;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::string::String;
use uuid;

/// A user of the application
#[derive(Getters, Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct User {
    /// Name of the user
    pub name: String,
    /// UUID of the medias on his collection
    pub medias: HashSet<uuid::Uuid>,
    ///  Unique Identifier of the user
    uid: uuid::Uuid,
}

impl User {
    pub fn new(name: String, medias: HashSet<uuid::Uuid>) -> Self {
        Self {
            name: name,
            medias: medias,
            uid: uuid::Uuid::new_v4(),
        }
    }
}
