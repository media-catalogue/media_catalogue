use serde::{Deserialize, Serialize};
use std::collections::{VecDeque};
use std::string::String;
use std::time::SystemTime;
use std::vec::Vec;
use std::fmt;
use derive_getters::Getters;
use uuid;

/// Reprresent a media
#[derive(Deserialize, Serialize,Hash, Debug, Clone, Eq, PartialEq, Getters)]
pub struct Media {
    /// name of the media
    pub name: String,
    /// chapters of the media
    pub chapters: Vec<Chapter>,
    /// Year of publication
    pub year: i8,
    /// Unique identifier of the media
    uuid: uuid::Uuid,
}

/// location of the a chapter
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub enum Location {
    Internet(String),
    LocalDrive(String),
}

#[derive(Deserialize, Serialize, Debug, Hash,
     Clone, Eq, PartialEq)]
pub struct Chapter {
    pub name: String,
    pub number: usize,
}
/// Represent the current chapter's the user is in and the history of consumption
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq, Default)]
pub struct Cursor {
    pub current: Option<Chapter>,
    pub history: VecDeque<(Chapter, SystemTime)>,
}

impl fmt::Display for Media {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-({})",self.name, self.year)
    }
}
