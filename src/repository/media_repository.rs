use super::{AddMedia, GetMedia};
use crate::domain::media::Media;
use crate::RepositoryError;
use derive_getters::Getters;
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use std::string::String;
use uuid;

#[derive(Getters, Serialize, Default)]
pub struct MediaRepository {
    #[serde(skip)]
    pub directory: Option<String>,
    medias: HashSet<Media>,
    #[serde(skip)]
    index: Indexes,
}

#[derive(Getters, Default)]
pub struct Indexes {
    pub uuid_index: HashMap<uuid::Uuid, *const Media>,
}
impl MediaRepository {
    pub fn new(directory: Option<String>, medias: HashSet<Media>) -> Self {
        let mut repo = Self {
            directory: directory,
            medias: medias,
            index: Indexes::default(),
        };
        repo.create_uuid_index();
        repo
    }

    fn create_uuid_index(&mut self) {
        self.index = Indexes {
            uuid_index: self
                .medias()
                .into_iter()
                .map(|v| (v.uuid().clone(), core::ptr::addr_of!(*v)))
                .collect(),
        };
    }
}

impl AddMedia for MediaRepository {
    fn add(&mut self, media: &Media) -> Result<(), RepositoryError> {
        if self.medias.contains(media) {
            Err(RepositoryError::DocumentAlreadyExist)
        } else {
            self.medias.insert(media.clone());
            self.create_uuid_index();
            Ok(())
        }
    }

    fn update(&mut self, media: &Media) -> Result<(), crate::RepositoryError> {
        if self.medias.contains(media) {
            self.medias.insert(media.clone());
        }

        Err(RepositoryError::DocumentDontExist)
    }
}

impl GetMedia for MediaRepository {
    fn get_all_medias(&self) -> Result<Vec<Media>, RepositoryError> {
        Ok(self.medias.iter().map(|v| v.clone()).collect())
    }

    fn get_media_by_uuid(&self, uuid: &uuid::Uuid) -> Result<Media, RepositoryError> {
        self.index
            .uuid_index
            .get(uuid)
            .map(|v| unsafe { (**v).clone() })
            .ok_or(RepositoryError::DocumentDontExist)
    }
}
