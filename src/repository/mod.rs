use crate::domain::media::Media;
use crate::RepositoryError;
pub mod media_repository;
mod test;
use uuid;

pub trait GetMedia {
    fn get_all_medias(&self) -> Result<Vec<Media>, RepositoryError>;
    fn get_media_by_uuid(&self, uuid: &uuid::Uuid) -> Result<Media, RepositoryError>;
}

pub trait AddMedia {
    fn add(& mut self, media: &Media) -> Result<(), RepositoryError>;
    fn update(& mut self, media: &Media) -> Result<(), RepositoryError>;
}

pub trait RepositoryBasicOperation {
    fn size(&self) -> Result<usize, RepositoryError>;
    fn save(&self) -> Result<(), RepositoryError>;
}

pub trait DeleteFromRepository {
    fn clear(&mut self) -> Result<(), RepositoryError>;
}
