#[cfg(test)]
mod tests_repository {
    use crate::repository::media_repository::MediaRepository;
    use crate::repository::GetMedia;

    mod test_get_media {

        use super::*;

        #[test]
        fn should_get_no_media_when_calling_get_all_medias_given_an_empty_repository() {
            let repo = MediaRepository::default();
            let media = repo.get_all_medias().unwrap();
            assert_eq!(media.len(), 0);
        }
    }
}
